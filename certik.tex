\documentclass[xcolor=svgnames]{beamer}
\usetheme{Stockton}

\usepackage{epsfig} %for figures
\usepackage{xcolor} %for color
\usepackage[utf8]{inputenc}
\usepackage{multicol}
\usepackage{listings}
\usepackage{textcomp}

%latex definitions:
\def\d{{\rm d}}
\def\half{{\textstyle{1\over2}}}



\title[LFortran\hspace{14em}\insertframenumber/
\inserttotalframenumber]{LFortran: Interactive LLVM-based Fortran Compiler for Modern Architectures}


\author[Čertík]{%
Ondřej Čertík\inst{1},
Gagandeep Singh,
Thirumalai Shaktivel,
Rohit Goswami,
Dominic Poerio,
Harris M. Snyder,
Andrew Best,
Jacob Marks,
Himanshu Pandey,
Carlos Une,
Nikhil Maan,
Sebastian Ehlert,
Ankit Pandey,
Mengjia Lyu,
Scot Halverson,
Laurence Kedward,
Milan Curcic%
}

\pgfdeclareimage[height=1.5cm]{mylogo}{images/lanl_logo}
\institute{%
Los Alamos National Laboratory\inst{1}\\
\vskip0.3cm \pgfuseimage{mylogo}}

\date{September 23, 2021}

\begin{document}

\begin{frame}
\maketitle
\end{frame}

\begin{frame}{Outline}
\begin{itemize}
    \item Background and Motivation
    \item fortran-lang
    \item Current Status
    \item Demo
    \item Architecture
    \item Future Plans
    \item Conclusions
\end{itemize}
\end{frame}

\begin{frame}{Background: Current state of Fortran}
Fortran is a powerful, easy-to-learn language for high-performance computing, however
\begin{itemize}
    \item It lacks modern tooling
    \item Has a poor ecosystem of libraries
    \item Difficult to run on GPUs
    \item Users are disconnected and re-inventing the wheel
\end{itemize}
$\Rightarrow$ User adoption and new Fortran projects are diminishing
\end{frame}

\begin{frame}{Toward a thriving open source Fortran community}
\begin{itemize}
    \item A rich standard library
    \item An easy-to-use, fast package manager and build system
    \item A cutting-edge, fast open source compiler and interpreter for CPUs, GPUs, and emerging architectures
    \item A modern, beautiful website and learning resources to attract new users
    \item A welcoming and inclusive online community
\end{itemize}
$\Rightarrow$ fortran-lang: A young and vibrant online community of Fortran developers
\end{frame}

\begin{frame}{Four pillars of fortran-lang}
\begin{enumerate}
    \item Standard Library (\url{https://github.com/fortran-lang/stdlib})
    \item Package Manager (\url{https://github.com/fortran-lang/fpm})
    \item Compilers -  open source and commercial
    \item Website (\url{https://fortran-lang.org})
\end{enumerate}
\end{frame}

\begin{frame}{Four pillars of fortran-lang (cont.)}
\begin{center}
\includegraphics[width=\textwidth]{images/fortran-lang.pdf}
\end{center}
\end{frame}

\begin{frame}{LFortran}
\begin{center}
\includegraphics[width=0.7\textwidth]{images/lfortran_logo_white_bg_500x500.png}
\end{center}
\end{frame}

\begin{frame}{LFortran: Motivation}
Everything one would expect from modern Fortran:
\begin{itemize}
    \item Cross platform compilation to binaries
    \item Interactivity (like Python)
    \item Nice error messages and warnings
    \item Automatic interoperability with other languages
    \item Translation to other languages
    \item Automatic formatting / language server (VSCode)
    \item Run well on modern architectures (GPU)
    \item Clean design, usable as a library
    \item Static analysis
    \item ...
\end{itemize}
As facilitated by a \textbf{modern compiler}!!
\end{frame}

\begin{frame}{History}
\begin{itemize}
    \item First commit on October 26, 2017
    \item Proof of concept written in Python
    \item First public announcement on April 30, 2019
    \item Minimum Viable Product (MVP) on September 23, 2021
\end{itemize}
\end{frame}


\begin{frame}{LFortran: Current Status}
\begin{itemize}
    \item Status page: https://docs.lfortran.org/progress/
    \item Production version written in C++
    \item Works interactively (Jupyter) and compiles to binaries with two
          backends (LLVM and x86)
    \item C++ translation backend
    \item Complete Fortran 2018 parser and formatter
    \item Can compile most features of Fortran 95 at least partially
    \item 3 GSoC students, 15 total contributors, 7030 commits, 1230 merged MRs
\end{itemize}
\end{frame}

\begin{frame}{LFortran MVP}
\vskip 0.1cm
    {\bf We are releasing MVP (Minimum Viable Product) today!}
\vskip 0.5cm
\begin{itemize}
    \item \texttt{conda install lfortran=0.14.0 jupyter}
    \item Blogpost: https://lfortran.org/blog/
    \item Demo code: https://gitlab.com/lfortran/examples/mvp\_demo
    \item Demo notebooks: https://gitlab.com/lfortran/lfortran/-/tree/master/share/lfortran/nb
    \item Alpha version
    \item Usable if you are willing to work around current limitations
    \item We are looking for new users, testers and contributors
\end{itemize}
\end{frame}


\begin{frame}
\fontsize{24pt}{15}\selectfont
\begin{center}
    {\bf Demo}
\end{center}
{\fontsize{12pt}{10}\selectfont Webpage: \url{https://lfortran.org}}
\end{frame}

\begin{frame}{Architecture}
\begin{center}
\includegraphics[width=\textwidth]{images/arch.pdf}
\end{center}
\vskip0.3in
AST: Abstract Syntax Tree\\
ASR: Abstract Semantic Representation\\
LLVM: modular and reusable compiler and toolchain technologies
\end{frame}


\begin{frame}{Architecture II}
\begin{itemize}
    \item AST and ASR are standalone independent representations
    \item User tools can target ASR or AST (easy to produce
        semantically correct Fortran code)
    \item ASR allows manipulating Fortran code similar to SymPy
    \item ASR allows rewriting / optimizing of the code (inlining, loop
        optimizations, ...) and showing the end result to the user as Fortran
        code
    \item One can also examine the LLVM code (although less readable than
        Fortran).
    \item Will make the compiler more transparent and helpful to the user
\end{itemize}
\end{frame}

\begin{frame}{Architecture III: Runtime Library}
\begin{itemize}
    \item Intrinsic functions, file IO, coarrays, etc.
    \item Written in Fortran
    \item Standalone library, works with any Fortran and C compiler
    \item Three sections: builtin, pure, impure
    \item Builtin: size, len, lbound, ... (must be implemented by the backend)
    \item Pure: pure Fortran, only depends on builtin or pure
    \item Impure: written in Fortran, calls into C via \texttt{iso\_c\_binding}
\end{itemize}
\end{frame}

\begin{frame}{Architecture IV: Intrinsic Math Functions (e.g., sin(x))}
We will have to versions:
\begin{itemize}
    \item Debug: Accuracy first, performance second
    \item Release: Performance first, accuracy second
\end{itemize}
\vskip 1cm
We plan to maintain the "Performance first" versions in pure Fortran as a
standalone library.
\end{frame}

\begin{frame}{LFortran: Future Plans}
Near term
\begin{itemize}
    \item Finish Fortran 95 compilation (SNAP and Dftatom projects)
\end{itemize}
Longer term:
\begin{itemize}
    \item Compile Fortran 2018 (including fixed-form and F77)
    \item Grow a community of developers
    \item Automatic Python and C++ wrappers
    \item Robust translation to C++
    \item Add other translation backends (Julia, Python/Numba, etc.)
    \item Runtime library: community maintained
    \item All the other items from the Motivation
\end{itemize}
\end{frame}


\begin{frame}
\fontsize{24pt}{15}\selectfont
\begin{center}
    {\bf Thank you!}
\end{center}
\end{frame}


\end{document}
