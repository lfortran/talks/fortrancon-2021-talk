# FortranCon 2021 Talk

Abstract and presentation about LFortran for FortranCon 2021.

* Abstract: [abstract.md](./abstract.md)
* Presentation slides: [certik.pdf](./certik.pdf)

Compile using:

    pdflatex certik.pdf
