# Title

LFortran: Interactive LLVM-based Fortran Compiler for Modern Architectures

# Authors

Ondřej Čertík, ondrej@certik.us, Los Alamos National Laboratory  
Gagandeep Singh, gdp.1807@gmail.com, Quansight  
Thirumalai Shaktivel, thirumalaishaktivel@gmail.com, KS Institute Of Technology  
Dominic Poerio, dominic@poer.io, Axalta Coating Systems  
Rohit Goswami, rog32@hi.is, University of Iceland  
Jacob Marks, bughunterstudios@gmail.com, Los Alamos National Laboratory  
Himanshu Pandey, himanshu7pandey7@gmail.com  
Nikhil Maan, nikhilmaan22@gmail.com, Amity University, India  
Andrew Best, andrew@blamsoft.com, Blamsoft, Inc.  
Sebastian Ehlert, sehlert@uni-bonn.de, University of Bonn  
Ankit Pandey, pandeyan@grinnell.edu, Grinnell College  
Scot Halverson, sah@lanl.gov, Los Alamos National Laboratory  
Laurence Kedward, laurence.kedward@bristol.ac.uk, University of Bristol  
Milan Curcic, milancurcic@hey.com, University of Miami  


# Abstract (200 words max)

We are developing a modern open-source Fortran compiler called [LFortran](https://lfortran.org/). This compiler enables the interactive execution of code in environments like Jupyter, enabling exploratory workflows in Fortran (like Python, MATLAB or Julia). Besides interactive use, LFortran can also generate binaries. A live demo of the compiler with a Jupyter notebook will be shown. The compiler is written in C++ for robustness and speed. It parses Fortran code to an Abstract Syntax Tree (AST) and transforms it to an Abstract Semantic Representation (ASR). LFortran includes several backends that transform the ASR to machine code via LLVM, x86, or C++ implementations. More backends are planned. Our compiler has been designed to be modular so that the AST and ASR can be used independently, which is an important feature for supporting an ecosystem of tools that otherwise would be hard with a monolithic compiler design. Any Fortran 2018 code can now be parsed into the AST, with smaller subset to ASR and an even smaller subset to LLVM and machine code. We are now working on extending the subset of code that can be compiled. The goal is to be Fortran 2018 compliant.
